#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "help.h"
/* There is a barber shop with a certain amount of barbers and a certain amount of
 * Waitingroom chairs. Our job is to make sure that if a customer arrives at the front
 * door and there is available chair in the waiting room then we let him in. But if
 * there is no available chair then we make him go away and he wont get a haircut.
 * We do this by having a semophore called "chairs" this semophore keeps track of
 * how many chairs are available in the waiting room. We use sem_post to inciment
 * the semohpore and sem_trywait to decrement and lock. The resone why we used sem_trywait
 * in stead of sem_wait is because that trywait returns -1 one if the semophore is 0 and it
 * returns 0 if the smophore is non-zero. That meant that we could use an if statement to
 * deside if a customer was let in the waiting room or not.
 *
 * We also have to make sure that if there are customers waiting and available barbers
 * then the barber has to start giving the customer next in line a haircut. We did this
 * by using a semophore called "customers_waiting". We called sem_wait at the start of
 * the main barber loop to make sure that we decremented the semophore to keep track
 * of customers waiting. We also called sem_post when custumers arrived in the
 * waiting room to increment the number of waiting customers.
 *
 * Every time a customer entered the waiting room or left it we had to change the
 * customer arrey. To make sure that only one customer(thread) could access the 
 * arrey we uset a semophore called "mutex" witch is a bimnary semophore, a lock.
 * We locked every time we were going to use data from customer and unlocked when
 * we finished.
 *
 */
/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below.
 *
 * === User information ===
 * User 1: hilmarr13 
 * SSN: 231092-2509
 * User 2: sindrik13
 * SSN: 220592-2359
 * === End User Information ===
 ********************************************************/

struct chairs
{
    struct customer **customer; /* Array of customers */
    int max;                 /*Number of chairs in waiting room */
    int front;               /*Index to the front of the customer queue */
    int rear;  		     /*Index to the end of the customer queue */
    sem_t mutex;	     /*Semophore for exlusion for the chairs variables */
    sem_t chair;             /*Semophore for avaialable chairs */
    sem_t customers_waiting; /*Semophore for the number of customers waiting*/
};

struct barber
{
    int room;
    struct simulator *simulator;
};

struct simulator
{
    struct chairs chairs;
    
    pthread_t *barberThread;
    struct barber **barber;
};

/*Error handling wrapper*/
void posix_error(int code, char *msg) /* Posix-style error */
{
    fprintf(stderr, "%s: %s\n", msg, strerror(code));
    exit(0);
}

/*Error handling wrapper*/
void Pthread_detach(pthread_t tid) {
    int rc;

    if((rc = pthread_detach(tid)) != 0) {
	posix_error(rc, "Pthread_detach error");
    }	
}

/*Error handling wrapper*/
void Pthread_create(pthread_t *tidp, pthread_attr_t *attrp, 
		    void * (*routine)(void *), void *argp) 
{
    int rc;

    if ((rc = pthread_create(tidp, attrp, routine, argp)) != 0)
	posix_error(rc, "Pthread_create error");
}
/* A function where the haircut takes place. The barber waits until there is a custumer waiting.
 * When a barber sees there is a customer waiting then we remove the customer at the front of the queue. */
static void *barber_work(void *arg)
{
    struct barber *barber = arg;
    struct chairs *chairs = &barber->simulator->chairs;
    struct customer *customer = 0; 

    /* Main barber loop */
    while (true) {
	/*The barber thread wait's until there is a customer waiting */
        sem_wait(&chairs->customers_waiting);

        sem_wait(&chairs->mutex); /*Let the thread get exclusion for the chairs variables */
	
	/*Choose the customer at the front of the queue and update the front variable*/
	customer = chairs->customer[(chairs->front++) % chairs->max]; 
	//chairs->free_chairs++; /* A new free chair is available */
	thrlab_prepare_customer(customer, barber->room); 
	
	/* The thread has finished updating the chairs variables and thus can let the mutex go*/
 	sem_post(&chairs->mutex); 
	sem_post(&chairs->chair);

	/*The customer undergoes the haircut and is then dismissed */
        thrlab_sleep(5 * (customer->hair_length - customer->hair_goal));
        thrlab_dismiss_customer(customer, barber->room);

	/*The customer has finished the haircut and can thus leave the barbershop */
	sem_post(&customer->mutex);
    }
    return NULL;
}

/**
 * Initialize data structures and create waiting barber threads.
 */
static void setup(struct simulator *simulator)
{
    struct chairs *chairs = &simulator->chairs;
    /* Setup semaphores*/
    sem_init(&chairs->mutex, 0, 1); /*Initalize the chair mutex at 1 so that the first thread can access it*/
    sem_init(&chairs->chair, 0, thrlab_get_num_chairs()); /*Initlaize the available chairs to the number of chairs*/
    sem_init(&chairs->customers_waiting, 0, 0); /*At first there are no customers waiting*/
    chairs->max = thrlab_get_num_chairs(); 
    /* Create chairs*/
    chairs->front = chairs->rear = 0;    
    chairs->customer = malloc(sizeof(struct customer *) * thrlab_get_num_chairs());
    
    /* Create barber thread data */
    simulator->barberThread = malloc(sizeof(pthread_t) * thrlab_get_num_barbers());
    simulator->barber = malloc(sizeof(struct barber*) * thrlab_get_num_barbers());

    /* Start barber threads */
    struct barber *barber;
    for (unsigned int i = 0; i < thrlab_get_num_barbers(); i++) {
	barber = calloc(sizeof(struct barber), 1);
	barber->room = i;
	barber->simulator = simulator;
	simulator->barber[i] = barber;
	Pthread_create(&simulator->barberThread[i], 0, barber_work, barber);
	Pthread_detach(simulator->barberThread[i]);
    }
}

/**
 * Free all used resources and end the barber threads.
 */
static void cleanup(struct simulator *simulator)
{
    /* Free chairs */
    free(simulator->chairs.customer);

    /* Free barber thread data */
    free(simulator->barber);
    free(simulator->barberThread);
}

/**
 * Called in a new thread each time a customer has arrived. For each customer that arrives, if there
 * is an available chair the customer sits down, otherwise he gets rejected.
 */
static void customer_arrived(struct customer *customer, void *arg)
{
    struct simulator *simulator = arg;
    struct chairs *chairs = &simulator->chairs;

    /*Initialize the customer mutex to 0 */
    sem_init(&customer->mutex, 0, 0);

   
    
    /*If there is a chair available we accept the customer, otherwise reject it.
     *If there is no free chair then trywait will return -1 otherwise it will return 0.
     *sem_trywait decrements the semophore just as sem_wait does. so we can keep count
     *of free chairs*/ 
    if(sem_trywait(&chairs->chair) == 0)  { 	
	
	/*Lock the chairs variables to ensure that two threads are not working with them at the same time */
    	sem_wait(&chairs->mutex);

	/*Assign the customer at the end of the queue */
        chairs->customer[(chairs->rear++) % chairs->max] = customer;

	/*Accept the customer and let the barber know that there is a customer waiting */
        thrlab_accept_customer(customer);

	/*Unlock the exclusion for the chairs variables */
        sem_post(&chairs->mutex);
	sem_post(&chairs->customers_waiting);

	/*Let the customer wait in the barbershop until he is finished with the haircut */
	sem_wait(&customer->mutex);
    }
    else {
        thrlab_reject_customer(customer);
    }
}



int main (int argc, char **argv)
{
    struct simulator simulator;

    thrlab_setup(&argc, &argv);
    setup(&simulator);

    thrlab_wait_for_customers(customer_arrived, &simulator);

    thrlab_cleanup();
    cleanup(&simulator);

    return EXIT_SUCCESS;
}
